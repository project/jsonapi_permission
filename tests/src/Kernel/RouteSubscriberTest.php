<?php

namespace Drupal\Tests\jsonapi_permission\Kernel;

use Drupal\KernelTests\KernelTestBase;

/**
 * Class RouteSubscriberTest.
 *
 * Tests the jsonapi_permission module.
 *
 * @package Drupal\Tests\jsonapi_permission
 *
 * @group jsonapi_permission
 */
class RouteSubscriberTest extends KernelTestBase {

  public static $modules = [
    'system',
    'user',
    'serialization',
    'jsonapi',
    'jsonapi_permission',
  ];

  protected function setUp() {
    parent::setUp();

    $this->installConfig(self::$modules);
  }

  /**
   * Check if the 'access jsonapi' is required for all routes starting with
   * jsonapi.
   */
  public function testPermissionsSetOnJSONAPIRoutes() {
    /**
     * @var $route_provider \Drupal\Core\Routing\RouteProviderInterface
     */
    $route_provider = \Drupal::service('router.route_provider');

    $routes = $route_provider->getAllRoutes();

    $this->assertNotCount(0, $routes);

    foreach ($routes as $key => $route) {
      if (stripos($key, 'jsonapi') !== 0) {
        continue;
      }
      $this->assertContains('access jsonapi', $route->getRequirement('_permission'));
    }

  }

}
