JSON:API Permission
===================

This module provides an 'Access JSON:API' permission to allow/disallow access to JSON:API routes.